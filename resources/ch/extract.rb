#!/usr/bin/env ruby

require 'CSV'

size_city = {}

CSV.foreach("gemeinden.csv") do |row|
  #puts "#{row[0]} / #{row[1]}"
  size_city[row[1].to_i] = row[0]#.gsub("(","").gsub(")","")
end

cities = size_city.sort_by {|k,v| k}.reverse.map{|k,v| v}
#puts cities.to_s

city_coords = {}
CSV.open("PLZO_CSV_WGS84.csv", "r", {:col_sep => ";"}).each do |row|
  # puts "#{row[0]} / #{row[6]},#{row[7]}"
  puts row.to_s if city_coords.has_key? row[3]
  city_coords[row[3]] = [] unless city_coords.has_key? row[3]
  city_coords[row[3]] << [row[5],row[7],row[6]]
end

result = []

cities.first(100).each{ |city|
  coords = city_coords[city]
  puts "#{city}: nil" if coords.nil?
  puts "#{city}: more than one" if ! coords.nil? && coords.size > 1
  unless coords.nil?
    coords.each { |c|
      url = "https://www.google.com/maps/search/?api=1&query=#{c[1]},#{c[2]}"
      result << [city,c,url].flatten
      puts url
    }
  end
  #puts "#{city}: #{coords[0]},#{coords[1]}" unless coords.nil?

}

CSV.open("city_data.csv", "wb") do |csv|
  # csv << ["row", "of", "CSV", "data"]
  # csv << ["another", "row"]
  result.each { |c|
    csv << c
  }
end
