package li.doerf.woist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_list_cities.*
import li.doerf.woist.fragments.FindCityActivityFragment
import li.doerf.woist.fragments.ListCitiesActivityFragment
import li.doerf.woist.fragments.ScoreFragment
import li.doerf.woist.objects.City
import li.doerf.woist.objects.Difficulty
import li.doerf.woist.objects.GameState
import li.doerf.woist.objects.Score

class ListCitiesActivity : AppCompatActivity() {
    private val LOGTAG = this::class.java.simpleName

    private lateinit var difficulty: Difficulty
    private lateinit var cities: List<City>
    private lateinit var scores: MutableList<Score>
    private var cityIndex: Int = 0
    private var state: GameState = GameState.STARTING

    private lateinit var listCitiesFragment: ListCitiesActivityFragment
    private lateinit var findCityFragment: FindCityActivityFragment

    companion object {
        val EXTRA_CITIES: String = "ExtraCities"
        private val EXTRA_DIFFICULTY: String = "ExtraDifficulty"

        fun newIntent(context: Context, cities: Array<City>, difficulty: Difficulty): Intent {
            val i = Intent(context, ListCitiesActivity::class.java)
            i.putExtra(ListCitiesActivity.EXTRA_CITIES, cities)
            i.putExtra(ListCitiesActivity.EXTRA_DIFFICULTY, difficulty)
            return i
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_cities)
        setSupportActionBar(toolbar)

        cities = (intent.getSerializableExtra(ListCitiesActivity.EXTRA_CITIES) as Array<City>).asList()
        difficulty = intent.getSerializableExtra(ListCitiesActivity.EXTRA_DIFFICULTY) as Difficulty

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        listCitiesFragment = ListCitiesActivityFragment.newFragment(this, cities.toTypedArray())
        supportFragmentManager.beginTransaction().add(R.id.fragment, listCitiesFragment).commit()

        fab.setOnClickListener {
            if ( state == GameState.STARTING ) {
                (application as WoistApplication).trackEvent("start_via_fab")
            }
            executeNextStep()
        }
    }

    private fun executeNextStep() {
        Log.d(LOGTAG, "state before: $state")
        when(state) {
            GameState.STARTING -> {
                scores = mutableListOf()
                showNextCity()
            }
            GameState.POSITION_MARKER -> measureResult()
            GameState.MEASURE_RESULT -> showNextCity()
            GameState.FINISH -> finishGame()
        }
        Log.d(LOGTAG, "state after: $state")
    }

    private fun showNextCity() {
        val nextCity = cities[cityIndex]
        Log.i(LOGTAG, "next city: $nextCity")
        findCityFragment = FindCityActivityFragment.newFragment(baseContext, nextCity, difficulty)
        supportFragmentManager.beginTransaction().replace(R.id.fragment, findCityFragment).commit()
        Log.d(LOGTAG, "fragment replaced")
        fab.setImageDrawable(ContextCompat.getDrawable(this, android.R.drawable.ic_menu_mylocation))
        state = GameState.POSITION_MARKER
    }

    private fun measureResult() {
        if ( ! findCityFragment.markerSet()) {
            val simpleAlert = AlertDialog.Builder(this).create()
            simpleAlert.setTitle(getString(R.string.marker_not_set_title))
            simpleAlert.setMessage(getString(R.string.marker_not_set_text))
            simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
                _, _ ->
                //
            })
            simpleAlert.show()
            return
        }

        val score = findCityFragment.calculateDistance()
        Log.i(LOGTAG, "score: $score")
        scores.add(score)

        cityIndex++
        fab.setImageDrawable(ContextCompat.getDrawable(this, android.R.drawable.ic_dialog_map))
        state = if ( cityIndex < cities.size ) GameState.MEASURE_RESULT else GameState.FINISH
    }

    private fun finishGame() {
        var totalScore = scores.map { s -> s.score }.sum()
        Log.i(LOGTAG, "total score: $totalScore")
        val scoreFragment = ScoreFragment.newInstance(totalScore, scores)
        supportFragmentManager.beginTransaction().replace(R.id.fragment, scoreFragment).commit()
        fab.visibility = View.GONE
    }

    /**
     * Reorder the cities list by starting with the city at the given position and continuing around
     */
    fun reorderCities(position: Int) {
        var newCities = cities.subList(position, cities.size).toMutableList()
        newCities.addAll(cities.subList(0, position))
        cities = newCities
        executeNextStep()
    }

}
