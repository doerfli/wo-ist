package li.doerf.woist.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Spinner
import li.doerf.woist.ListCitiesActivity
import li.doerf.woist.MainActivity
import li.doerf.woist.R
import li.doerf.woist.WoistApplication
import li.doerf.woist.objects.City
import li.doerf.woist.objects.Difficulty
import org.apache.commons.csv.CSVFormat
import java.io.InputStreamReader
import java.util.*

/**
 * A placeholder fragment containing a simple view.
 */
class MainActivityFragment : androidx.fragment.app.Fragment() {
    private val LOGTAG = MainActivity::class.java.simpleName
    private val NUMBER_OF_CITIES = 5
    private lateinit var cities: MutableList<City>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        val difficultySelector = view.findViewById<Spinner>(R.id.difficulty)
        difficultySelector.setSelection(1)
        var showMapButton = view.findViewById<Button>(R.id.start)
        showMapButton.setOnClickListener {
            Log.d(LOGTAG, "button clicked")
            val difficulty = extractDifficulty(difficultySelector)
            var citiesToFind = selectCities(difficulty)
            (activity!!.application as WoistApplication).trackEvent("start_game_$difficulty")
            startActivity(ListCitiesActivity.newIntent(context!!, citiesToFind.toTypedArray(), difficulty))
        }
        return view
    }

    private fun extractDifficulty(difficultySelector: Spinner): Difficulty {
        when (difficultySelector.selectedItemPosition) {
            0 -> return Difficulty.EASY
            1 -> return Difficulty.MEDIUM
            2 -> return Difficulty.HARD
        }

        throw IllegalStateException("unknown position: " + difficultySelector.selectedItemPosition)
    }

    private fun selectCities(difficulty: Difficulty): List<City> {
        val citiesToFind = when (difficulty) {
            Difficulty.EASY -> cities.subList(0, 30)
            Difficulty.MEDIUM -> cities.subList(0, 70)
            Difficulty.HARD -> cities.subList(0, 100)
        }

        Collections.shuffle(citiesToFind)
        return citiesToFind.subList(0, NUMBER_OF_CITIES)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadCityData()
    }

    override fun onResume() {
        super.onResume()
        (activity!!.application as WoistApplication).trackView(this.javaClass.simpleName)
    }

    private fun loadCityData() {
        val inStream = InputStreamReader(resources.openRawResource(R.raw.city_data))
        cities = mutableListOf()
        for (record in CSVFormat.DEFAULT.parse(inStream)) {
            val c = City(record[0], record[1], record[2].toDouble(), record[3].toDouble())
            cities.add(c)
            Log.d(LOGTAG, "Loaded city $c")
        }
    }
}
