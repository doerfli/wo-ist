package li.doerf.woist.fragments

import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import li.doerf.woist.R
import li.doerf.woist.WoistApplication
import li.doerf.woist.objects.Score
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ScoreFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ScoreFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ScoreFragment : androidx.fragment.app.Fragment(), OnMapReadyCallback {
    private val LOGTAG = ScoreFragment::class.java.simpleName
    private var score: Int = 0
    private lateinit var scores: Array<Score>
    private lateinit var scoreTextView: TextView
    private lateinit var map: GoogleMap

    companion object {
        private val ARG_SCORE = "score"
        private val ARG_SCORES = "scores"

        fun newInstance(score: Int, scores: MutableList<Score>): ScoreFragment {
            val fragment = ScoreFragment()
            val args = Bundle()
            args.putInt(ARG_SCORE, score)
            args.putSerializable(ARG_SCORES, scores.toTypedArray())
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            score = arguments!!.getInt(ARG_SCORE)
            scores = arguments!!.getSerializable(ARG_SCORES) as Array<Score>
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_score, container, false)

        scoreTextView = view.findViewById(R.id.score)

        val button = view.findViewById<Button>(R.id.finish)
        button.setOnClickListener{ activity?.finish() }

        val gmap = (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment)
        gmap.getMapAsync(this)

        return view
    }

    override fun onResume() {
        super.onResume()
        (activity?.application as WoistApplication).trackView(this.javaClass.simpleName)
        (activity?.application as WoistApplication).trackEvent("score_$score")
        val t = score.toString() + " " + getString(R.string.your_score_points)
        scoreTextView.text = t
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val s = Scanner(resources.openRawResource(R.raw.maps_style_more_details_with_labels)).useDelimiter("\\A")
            val mapStyles = if (s.hasNext()) s.next() else ""
            val success = map.setMapStyle(MapStyleOptions(mapStyles))

            if (!success) {
                Log.e(LOGTAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(LOGTAG, "Can't find style. Error: ", e)
        }

        for ( score in scores ) {
            // city marker
            val cityLatLng = LatLng(score.city.lat, score.city.lng)
            googleMap.addMarker(
                    MarkerOptions()
                            .position(cityLatLng)
                            .title(score.city.name)
                            .draggable(true)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            )

            // position marker
            googleMap.addMarker(
                    MarkerOptions()
                            .position(score.position)
                            .title(score.city.name)
                            .draggable(true)
            )
        }

        // Add a marker in middle and move the camera
        val middle = LatLng(46.801111, 8.226667)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(middle, 6.8f))
    }
}
