package li.doerf.woist.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import li.doerf.woist.ListCitiesActivity
import li.doerf.woist.R
import li.doerf.woist.WoistApplication
import li.doerf.woist.objects.City

/**
 * A placeholder fragment containing a simple view.
 */
class ListCitiesActivityFragment : androidx.fragment.app.Fragment() {

    private val LOGTAG = ListCitiesActivityFragment::class.java.simpleName

    private lateinit var cities: List<City>
    private lateinit var citiesListView: ListView
    private lateinit var parentActivity: ListCitiesActivity

    companion object {
        fun newFragment(activity: ListCitiesActivity, cities: Array<City>): ListCitiesActivityFragment {
            var fragment = ListCitiesActivityFragment()
            fragment.setActivity(activity)
            var args = Bundle()
            args.putSerializable(ListCitiesActivity.EXTRA_CITIES, cities)
            fragment.arguments = args
            return fragment
        }
    }


    private fun setActivity(anActivity: ListCitiesActivity) {
        parentActivity = anActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list_cities, container, false)

        citiesListView = view.findViewById(R.id.cities_list)
        citiesListView.setOnItemClickListener { parent, view, position, id ->
            Log.d(LOGTAG, "clicked city: $position")
            (activity!!.application as WoistApplication).trackEvent("start_via_citylist")
            parentActivity.reorderCities(position)
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cities = (arguments?.getSerializable(ListCitiesActivity.EXTRA_CITIES) as Array<City>).asList()

        for (i in cities.indices) {
            val city = cities[i]
            Log.d(LOGTAG, "$i - $city")
        }

        citiesListView.adapter = ArrayAdapter(context!!, android.R.layout.simple_list_item_1, android.R.id.text1, cities.map{ it.name })
    }

    override fun onResume() {
        super.onResume()
        (activity!!.application as WoistApplication).trackView(this.javaClass.simpleName)
    }

}
