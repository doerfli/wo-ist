package li.doerf.woist.fragments

import android.content.Context
import android.content.res.Resources
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import li.doerf.woist.R
import li.doerf.woist.WoistApplication
import li.doerf.woist.objects.City
import li.doerf.woist.objects.Difficulty
import li.doerf.woist.objects.Score
import java.util.*


/**
 * A placeholder fragment containing a simple view.
 */
class FindCityActivityFragment : androidx.fragment.app.Fragment(), OnMapReadyCallback {
    private val LOGTAG = FindCityActivityFragment::class.java.simpleName

    private lateinit var city: City
    private lateinit var difficulty: Difficulty

    private lateinit var map: GoogleMap
    private var myMarker: Marker? = null
    private var myTargetMarker: Marker? = null
    private lateinit var cityNameTextView: TextView
    private lateinit var resultTextView: TextView

    companion object {
        val EXTRA_CITY: String = "city"
        val EXTRA_DIFFICULTY: String = "difficulty"
        val MAX_SCORE: Int = 50
        val FULL_SCORE_RADIUS = 3

        fun newFragment(context: Context, city: City, difficulty: Difficulty): FindCityActivityFragment {
            var fragment = FindCityActivityFragment()
            var args = Bundle()
            args.putSerializable(EXTRA_CITY, city)
            args.putSerializable(EXTRA_DIFFICULTY, difficulty)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_find_city, container, false)

        cityNameTextView = view.findViewById(R.id.city_name)
        resultTextView = view.findViewById(R.id.result)
        
        val map = (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment)
        map.getMapAsync(this)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        city = arguments?.getSerializable(EXTRA_CITY) as City
        difficulty = arguments?.getSerializable(EXTRA_DIFFICULTY) as Difficulty
        cityNameTextView.text = city.name
    }

    override fun onResume() {
        super.onResume()
        (activity!!.application as WoistApplication).trackView(this.javaClass.simpleName)
        resultTextView.visibility = View.GONE
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        setMapStyle(when(difficulty) {
            Difficulty.EASY -> R.raw.maps_style_easy
            Difficulty.MEDIUM -> R.raw.maps_style_easy
            Difficulty.HARD -> R.raw.maps_style_hard
        })

        // Add a marker in middle and move the camera
        val middle = LatLng(46.801111, 8.226667)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(middle, 7f))

        map.setLatLngBoundsForCameraTarget(LatLngBounds(LatLng(45.818031, 5.956303), LatLng(47.808264, 10.491944)))

        map.setOnMapClickListener { latLng ->
            Log.d(LOGTAG, latLng.latitude.toString())
            Log.d(LOGTAG, latLng.longitude.toString())

            if (myMarker == null) {
                myMarker = googleMap.addMarker(
                        MarkerOptions()
                                .position(latLng)
                                .title(city.name)
                                .draggable(true)
                )
            }

            myMarker?.position = latLng
        }
    }

    private fun setMapStyle(styleId: Int) {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val s = Scanner(resources.openRawResource(styleId)).useDelimiter("\\A")
            val mapStyles = if (s.hasNext()) s.next() else ""
            val success = map.setMapStyle(MapStyleOptions(mapStyles))

            if (!success) {
                Log.e("MapLongClickListener", "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e("MapLongClickListener", "Can't find style. Error: ", e)
        }
    }

    fun calculateDistance(): Score {
        if (myMarker == null) {
            // TODO show warning and don't change state
            throw IllegalStateException("marker not set")
        }

        // calculate distance
        val targetLatLng = LatLng(city.lat, city.lng)
        Log.d(LOGTAG, "targetLatLng = $targetLatLng")
        val distance: FloatArray = floatArrayOf(0f)
        Location.distanceBetween(targetLatLng.latitude, targetLatLng.longitude, myMarker?.position?.latitude as Double, myMarker?.position?.longitude as Double, distance)
        val distanceKm = (distance[0] / 1000)
        Log.d(LOGTAG, "d = " + distanceKm)

        // calculate score
        val score = calculateScore(distanceKm)

        showResult(score)

        return score
    }

    private fun calculateScore(distanceKm: Float): Score {
        var scorePts = MAX_SCORE
        if (distanceKm > FULL_SCORE_RADIUS) {
            scorePts = scorePts + FULL_SCORE_RADIUS - Math.ceil(distanceKm.toDouble()).toInt()
        }
        if (scorePts < 0) scorePts = 0
        return Score(city, scorePts, myMarker?.position as LatLng, distanceKm)
    }

    private fun showResult(score: Score) {
        // set text on screen
        val s = when {
            score.distanceKm < FULL_SCORE_RADIUS -> getString(R.string.city_found, score.city.name)
            score.score <= 0 -> getString(R.string.distance_is_too_far, score.city.name, score.distanceKm)
            else -> getString(R.string.distance_is, score.city.name, score.distanceKm)
        }
        val bgColor = when {
            score.distanceKm < FULL_SCORE_RADIUS -> ContextCompat.getColor(context!!, R.color.match_success)
            score.score <= 0 -> ContextCompat.getColor(context!!, R.color.match_fail)
            else -> ContextCompat.getColor(context!!, R.color.match_almost)
        }

        val ptsTxt = getString(R.string.points_awarded, score.score)
        val t = "$s $ptsTxt"

        resultTextView.text = t
        resultTextView.visibility = View.VISIBLE
        resultTextView.setBackgroundColor(bgColor)

        val targetLatLng = LatLng(score.city.lat, score.city.lng)

        myTargetMarker = map.addMarker(MarkerOptions()
                .position(targetLatLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))

        // draw circles on map
        val colors = intArrayOf(R.color.target_circle_fill_0, R.color.target_circle_fill_1, R.color.target_circle_fill_2, R.color.target_circle_fill_3, R.color.target_circle_fill_4, R.color.target_circle_fill_5)
        for (i in (0..5)) {
            val r = (FULL_SCORE_RADIUS + i * 10).toDouble() * 1000
            val zIndex = (5 - i).toFloat()
            val color = ContextCompat.getColor(context!!, colors[i])
            map.addCircle(CircleOptions()
                    .center(targetLatLng)
                    .radius(r)
                    .strokeWidth(1f)
                    .strokeColor(ContextCompat.getColor(context!!, R.color.target_circle_stroke))
                    .fillColor(color)
                    .zIndex(zIndex)
            )
        }

        // zoom map to show both both markers and disable clicking
        val viewW = Math.min(targetLatLng.latitude, myMarker?.position?.latitude as Double)
        val viewE = Math.max(targetLatLng.latitude, myMarker?.position?.latitude as Double)
        val viewS = Math.min(targetLatLng.longitude, myMarker?.position?.longitude as Double)
        val viewN = Math.max(targetLatLng.longitude, myMarker?.position?.longitude as Double)
        val cu = CameraUpdateFactory.newLatLngBounds(LatLngBounds(LatLng(viewW, viewS), LatLng(viewE, viewN)), 350)
        map.moveCamera(cu)
        setMapStyle(R.raw.maps_style_more_details_with_labels)
        map.setOnMapClickListener {
            // nothing
        }
    }

    fun markerSet(): Boolean {
        return myMarker != null
    }
}
