package li.doerf.woist

import android.os.Bundle
import android.provider.Settings
import androidx.multidex.MultiDexApplication
import com.google.firebase.analytics.FirebaseAnalytics


/**
 * Created by moo on 12.11.17.
 */
class WoistApplication : MultiDexApplication() {
    private val LOGTAG = WoistApplication::class.java.simpleName
    private var firebaseAnalytics: FirebaseAnalytics? = null


    override fun onCreate() {
        super.onCreate()
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }

    @Synchronized
    fun trackView(name: String) {
        if (runsInTestlab()) return
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, name)
        bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "View")
        firebaseAnalytics!!.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle)
    }

    @Synchronized
    fun trackEvent(name: String) {
        if (runsInTestlab()) return
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, name)
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Function")
        firebaseAnalytics!!.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
    }

    /**
     * Check if the app runs in Android Firebase Test Lab (https://firebase.google.com/docs/test-lab/)
     */
    fun runsInTestlab(): Boolean {
        val testLabSetting = Settings.System.getString(this.contentResolver, "firebase.test.lab")
        return "true" == testLabSetting
    }

}