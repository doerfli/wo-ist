package li.doerf.woist.objects

/**
 * Created by moo on 12.11.17.
 */
enum class Difficulty {
    EASY, MEDIUM, HARD
}