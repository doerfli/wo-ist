package li.doerf.woist.objects

import com.google.android.gms.maps.model.LatLng

/**
 * Created by moo on 24.10.17.
 */
data class Score(val city: City, val score: Int, val position: LatLng, val distanceKm: Float)