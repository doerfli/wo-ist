package li.doerf.woist.objects

import java.io.Serializable

data class City(val name: String, val state: String, val lat: Double, val lng: Double): Serializable