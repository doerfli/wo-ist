package li.doerf.woist.objects

/**
 * Created by moo on 22.10.17.
 */
enum class GameState { STARTING, POSITION_MARKER, MEASURE_RESULT, FINISH }